package tech.master.turmas;

import java.time.LocalDate;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import feign.FeignException;

@Service
public class TurmaService {
    @Autowired
    private CursoClient cursoClient;
    @Autowired
    private TurmaRepository repository;

    Logger logger = LoggerFactory.getLogger(TurmaService.class);

    public Iterable<Turma> listar(){
        return repository.findAll();
    }

    @HystrixCommand(fallbackMethod = "turmaDefault")
    public Turma criar(int idCurso) {
   
            Curso curso = cursoClient.buscar(idCurso);

            Turma turma = new Turma();
            turma.setNomeCurso(curso.getNome());
            turma.setDataInicio(LocalDate.now());

            return repository.save(turma);
       
    }
    
    public Turma turmaDefault(int idCurso) {
    	Curso curso = new Curso();
    	curso.setNome("A ser definido");
    	curso.setId(idCurso);
    	
    	Turma turma = new Turma();
    	turma.setNomeCurso(curso.getNome());
    	turma.setDataInicio(LocalDate.now());
    	
    	return repository.save(turma);
    }
}
